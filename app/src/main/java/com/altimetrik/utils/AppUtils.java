package com.altimetrik.utils;

import android.app.Activity;
import android.support.design.widget.Snackbar;

/**
 * Created by nishantdande on 24/09/16.
 */

public class AppUtils {

    public static void displayShortSnackbar(Activity activity,String s){
        Snackbar.make(activity.findViewById(android.R.id.content), s, Snackbar.LENGTH_LONG).show();
    }

    public static boolean isAlphaNumeric(String s){
        String pattern= "^[a-zA-Z0-9]*$";
        if(s.matches(pattern)){
            return true;
        }
        return false;
    }
}
