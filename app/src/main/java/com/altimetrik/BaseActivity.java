package com.altimetrik;

import android.support.annotation.StringRes;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.altimetrik.utils.AppUtils;

public class BaseActivity extends AppCompatActivity {

    /**
     * Show Error in Snakebar
     * @param msg
     */
    protected void showError(String msg){
        AppUtils.displayShortSnackbar(this,msg);
    }
}
