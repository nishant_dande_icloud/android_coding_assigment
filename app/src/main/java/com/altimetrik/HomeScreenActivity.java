package com.altimetrik;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by nishantdande on 22/10/16.
 */

public class HomeScreenActivity extends BaseActivity {

    @BindView(R.id.username)
    TextView mUsername;

    @BindView(R.id.profile_pic)
    CircleImageView mProfilepic;


    private final String URl = "http://52.27.246.223:8080/api/members/photo/get/1.0/68c7daac-8a1b-4445-a865-d9ce3b7cfc58/188a769f-1268-4f9e-9d56-8c5306fab647.jpg";
    Bundle bundle;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_screen);
        ButterKnife.bind(this);

        bundle = getIntent().getExtras();
        if (bundle!= null){
            String s = bundle.getString("username");
            mUsername.setText(getString(R.string.welcome_s, s));
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        setProfilePic();
    }

    private void setProfilePic() {
        Glide.with(this)
                .load(URl)
                .centerCrop()
                .placeholder(R.mipmap.ic_launcher)
                .crossFade()
                .dontAnimate()
                .into(mProfilepic);
    }
}
